from random import gauss, seed, randrange
import random
import multiprocessing as mp
from os import cpu_count
import json

import pandas as pd
import gym
import numpy as np
from datetime import datetime, timedelta

from IPython.display import clear_output
import pdb


# Based on code from https://www.learndatasci.com/tutorials/reinforcement-q-learning-scratch-python-openai-gym/


"""Training the agent"""


# learner hyperparameters

def linear_epsilon(epoch_count):
    initial_epsilon = randrange(5.0, 20.0, 1.0)/100
    final_epsilon = randrange(1.0, 5.0, 1.0)/100
    #if (final_epsilon > initial_epsilon):
    #    initial_epsilon, final_epsilon = final_epsilon, initial_epsilon
    #div = (epoch_count/1000)
    epsilon_slope = (final_epsilon-initial_epsilon)/epoch_count
    return initial_epsilon, final_epsilon, epsilon_slope


def linear_alpha(epoch_count):
    initial_alpha = randrange(1.0, 25.0, 1.0)/100
    final_alpha = randrange(1.0, 25.0, 1.0)/100
    #if (final_alpha > initial_alpha):
    #    initial_alpha, final_alpha = final_alpha, initial_alpha
    #div = (epoch_count/1000)
    alpha_slope = (final_alpha-initial_alpha)/epoch_count
    return initial_alpha, final_alpha, alpha_slope


def linear_gamma(epoch_count):
    initial_gamma = randrange(50.0, 99.0, 1.0)/100
    final_gamma = randrange(50.0, 99.0, 1.0)/100
    #if (final_gamma > initial_gamma):
    #    initial_gamma, final_gamma = final_gamma, initial_gamma
    #div = (epoch_count/1000)
    gamma_slope = (final_gamma-initial_gamma)/epoch_count
    return initial_gamma, final_gamma, gamma_slope




# environmental reward modifiers

def make_chaos_level():
    chaos_level = randrange(start=0.0, stop=20.0, step=1.0)/100
    return chaos_level


def chaos_mod(chaos_level):
    if (random.uniform(0,1) < chaos_level):
        mod = gauss(0.0, chaos_level)
    else:
        mod = 0
    return mod


def calculate_reward(base_reward, chaos_level):
    percent_diff = chaos_mod(chaos_level)
    adjusted_reward = base_reward*(1+percent_diff)
    return adjusted_reward


def update_running_average(counter_list, averages_list, value):
    if (len(counter_list) < 1000):
        average = sum(counter_list)/len(counter_list)
    else:
        average = sum(counter_list[-1000:])/len(counter_list[-1000:])
    averages_list.append(average)
    return



def train_linear_learner(chaos=True):
    
    start = dt.now()
    
    epoch_count = 60000
    
    env = gym.make("Taxi-v3").env
    
    #Initialize Q table
    q_table = np.zeros([env.observation_space.n, env.action_space.n])
        
    epsilon, final_epsilon, epsilon_slope = linear_epsilon(epoch_count)
    alpha, final_alpha, alpha_slope = linear_alpha(epoch_count)
    gamma, final_gamma, gamma_slope = linear_gamma(epoch_count)
    
    #print(alpha, final_alpha, alpha_slope)
    #print(epsilon, final_epsilon, epsilon_slope)
    #print(gamma, final_gamma, gamma_slope)
    
    if chaos:
        chaos_level = make_chaos_level()
    else:
        chaos_level = 0.0
    
    
    # For plotting metrics
    # these are downsampled to just every 25th epoch
    epoch_step_counts = []
    base_reward_totals = []
    custom_reward_totals = []
    penalty_counts = []
    
    
    metadata = {
        "initial_alpha": alpha,
        "final_alpha": final_alpha,
        "alpha_slope": alpha_slope,
        "initial_epsilon": epsilon,
        "final_epsilon": final_epsilon,
        "epsilon_slope": epsilon_slope,
        "initial_gamma": gamma,
        "final_gamma": final_gamma,
        "gamma_slope": gamma_slope,
        "chaos_level": chaos_level,
        "epoch_step_counts": epoch_step_counts,
        "base_reward_totals": base_reward_totals,
        "custom_reward_totals": custom_reward_totals,
        "penalty_counts": penalty_counts,
        #"q_table": q_table
        }
    
    
    for i in range(1, (epoch_count + 1)):
        state = env.reset()

        epochs, penalties, reward = 0, 0, 0
        
        step_count = 0
        net_base_reward = 0
        net_custom_reward = 0
        
        done = False

        while not done:
            
            if random.uniform(0, 1) < epsilon:
                action = env.action_space.sample() # Explore action space
            else:
                action = np.argmax(q_table[state]) # Exploit learned values

            next_state, reward, done, info = env.step(action)
            

            old_value = q_table[state, action]
            next_max = np.max(q_table[next_state])
            
            custom_reward = calculate_reward(reward, chaos_level)

            new_value = (1 - alpha) * old_value + alpha * (custom_reward + gamma * next_max)

            q_table[state, action] = new_value


            if reward == -10:
                penalties += 1

            net_base_reward += reward
            net_custom_reward += custom_reward
            step_count += 1
            
        
            state = next_state
            
            
        # update hparams and params for next epoch
        alpha += alpha_slope
        epsilon += epsilon_slope
        gamma += gamma_slope
           
        base_reward_totals.append(net_base_reward)
        custom_reward_totals.append(net_custom_reward)
        epoch_step_counts.append(step_count)
        penalty_counts.append(penalties)
        
        latest = dt.now()
        training_time = latest-start
        metadata["training_time"] = str(training_time)

        if training_time > timedelta(minutes=10):
            return None
        
        
            #print(f"Episode: {i}")

    return metadata


    
def linear_sim_no_chaos():
    result = train_linear_learner(chaos=False)
    filename = "./simulation_data/linear_learners_no_chaos.json"
    return result, filename


def linear_sim_with_chaos():
    result = train_linear_learner(chaos=True)
    filename = "./simulation_data/linear_learners_with_chaos.json"
    return result, filename

from datetime import datetime as dt
print(dt.now())



if __name__ == "__main__":
    start = datetime.now()
    #cpus = cpu_count() - 1
    procs = 3
    N = 1000
    
    print(start)
    print("Starting {} linear sims in {} processes.".format(N, procs))
    with mp.Pool(processes=procs) as pool:
        
        for i in range(N):
        
            result, filename = pool.apply_async(linear_sim_no_chaos).get() 
            with open(filename, "a") as dest:
                dest.write(json.dumps(result) + '\n')

                
            #result, filename = pool.apply_async(linear_sim_with_chaos).get()
            #with open(filename, "a") as dest:
            #    dest.write(json.dumps(result) + '\n')

        
    print("Finishing sims")
    end = datetime.now()
    print("{} sims run in {}.".format(str(2*N), str(end-start)))