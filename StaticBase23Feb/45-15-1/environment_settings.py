#encoding: utf-8

from random import gauss, seed, randrange, uniform
import gym
import os

environment_success = 45
environment_penalty = -15
environment_maintenance = -1

base_reinforcement_settings = (environment_success, environment_penalty, environment_maintenance)
env_base = f"{environment_success}{environment_penalty}{environment_maintenance}"

environment_epoch_count = 100000

key_epochs = [1, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 
              10000, 15000, 20000, 25000, 30000, 35000, 40000, 45000, 50000, 
              55000, 60000, 65000, 70000, 75000, 80000, 85000, 90000, 95000, 100000]




def make_responsiveness():
    responsiveness = randrange(start=1.0, stop=100.0, step=1.0)/100
    return responsiveness

def make_harshness():
    harshness = randrange(start=1.0, stop=100.0, step=1.0)/100
    return harshness


def make_chaos_level():
    chaos_level = randrange(start=0.0, stop=30.0, step=1.0)/100
    return chaos_level

def chaos_mod(chaos_level):
    if (uniform(0,1) < chaos_level):
        mod = (1.0 + gauss(0.0, chaos_level))
    else:
        mod = 1.0
    return mod



def make_filenames(flavor, unique_id):
    
    bins = [f"{i}000"for i in range(10)]
    
    metadata_end = f"{flavor}_{env_base}_{unique_id}.json"
    qtable_end = f"Q_table_{flavor}_{env_base}_{unique_id}.json"
    
    for thou in bins:
        
        metadata_subdir = f"./simulation_data/{flavor}/metadata/{thou}"
        qtable_subdir = f"./simulation_data/{flavor}/q_tables/{thou}"
    
        if os.path.exists(metadata_subdir):
            filecount = len(os.listdir(metadata_subdir))
            if filecount >= 1000:
                continue
            else:
                break
        else:
            os.mkdir(metadata_subdir)
            os.mkdir(qtable_subdir)
            break
    
    metadata_path = os.path.join(metadata_subdir, metadata_end)    
    qtable_path = os.path.join(qtable_subdir, qtable_end)
    
    return metadata_path, qtable_path