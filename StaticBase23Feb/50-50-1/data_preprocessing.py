# encoding: utf-8


import random
from statistics import stdev, variance, mean, StatisticsError
import pdb

from environment_settings import base_reinforcement_settings, chaos_mod



base_success, base_penalty, base_maintenance = base_reinforcement_settings


def bespoke_base_reward(reward):
    if reward == 20:
        return base_success
    elif reward == -10:
        return base_penalty
    elif reward == -1:
        return base_maintenance
    else:
        pdb.set_trace()
        raise Exception


def calculate_feedback(reward, 
                       chaos_level, 
                       harshness, 
                       responsiveness):
    
    base_reward = bespoke_base_reward(reward)
    
    ## Chaos Mod: an envelope around all the responses from an environment
    adjusted_reward = base_reward * chaos_mod(chaos_level)
    
    penalty_incr = 0
    penalty_value = None
    maintenance_value = None
    success_value = None
    

    ### Here, reward responsiveness and penalty harshness are orthogonal

    #### NOTE!!
    #### The {-1, -10, +20} reward schema holds for the *stock* Taxi V3 environment, only.
    ####
    #### The variables at the top of this file set the values for
    #### bespoke_base_reward
    #### Make sure to revisit this for each environment, as needed
    
    
    if (base_reward == base_maintenance): 
        
        ### MAINTENANCE CONDITION
        maintenance_value = adjusted_reward
        
        
    elif (base_reward == base_success): 
        
        ### SUCCESS CONDITION
        ### RESPONSIVENESS/UNAVAILABILITY MODIFIER
        # check if things are randomly unresponsive
        # higher responsiveness -> greater chance of normal reward
        # lower responsiveness -> greater chance of reduced reward & greater reduction of value
        
        unreliability = 1-responsiveness
        random_unavailability = random.uniform(0,1)
        if (random_unavailability > responsiveness):
            modifier = 2*random.uniform(0, unreliability)  # avg modifier == unreliability
            adjusted_reward = adjusted_reward * (1-modifier)

        success_value = adjusted_reward

        
    elif (base_reward == base_penalty):

        
        ### PENALTY CONDITION
        ### FAIRNESS/HARSHNESS MODIFIER
        # check if things are randomly extra-punitive
        # higher harshness -> greater chance of extra penalty and greater reduction to reward for it

        fairness = 1-harshness
        random_punishment = random.uniform(0,1)
        if (random_punishment > fairness): 
            modifier = 2*random.uniform(0, harshness)  # avg modifier == harshness
            adjusted_reward = (adjusted_reward * (1+modifier))   
        
        penalty_value = adjusted_reward
        penalty_incr = 1

            
    feedback = {
        "custom_reward": adjusted_reward,
        "penalty_incr": penalty_incr,
        "penalty_value": penalty_value,
        "maintenance_value": maintenance_value,
        "success_value": success_value,
    }

    
    return feedback



def update_metrics_with_feedback(feedback, epoch_data):
        
    epoch_data["step_count"] += 1                              # always!
    epoch_data["net_reward"] += feedback["custom_reward"]      # always!
    
    if feedback["penalty_incr"] == 1:
        epoch_data["penalty_count"] += feedback["penalty_incr"]
        epoch_data["penalty_cost"] += feedback["penalty_value"]
        epoch_data["penalty_values"].append(feedback["penalty_value"])

    elif feedback["maintenance_value"] != None:
        epoch_data["maintenance_cost"] += feedback["maintenance_value"]
        epoch_data["maintenance_values"].append(feedback["maintenance_value"])

    elif feedback["success_value"] != None:
        epoch_data["success_value"] += feedback["success_value"]
       
    
    return
    

    
def record_penalty_free_epochs(current_epoch,
                               epoch_penalty_counts,
                               penalty_free_epoch_runs):
    
    for n in penalty_free_epoch_runs.keys():
        if ((penalty_free_epoch_runs[n] == None) and
            (current_epoch >= n)):
            if sum(epoch_penalty_counts[-n:]) == 0:
                penalty_free_epoch_runs.update({n: current_epoch})
    return


def penalty_free_epoch_density(epoch_penalty_counts):
    last_100 = epoch_penalty_counts[-100:]
    free = last_100.count(0)
    percent = free/100
    return percent



def mean_and_sdev(metric):
    try:
        mean_ = mean(metric)
    except StatisticsError:
        mean_ = None
        
    try:
        sdev_ = stdev(metric)
    except StatisticsError:
        sdev_ = None
        
    return mean_, sdev_


def last_100_mean_and_sdev(metrics_list):
    last_100 = metrics_list[-100:]
    if type(last_100[0]) == list:
        aggregate = []
        for e in last_100:
            aggregate = aggregate + e
    else:
        aggregate = last_100
            
    try:
        mean_agg = mean(aggregate)
    except StatisticsError:
        mean_agg = None
        
    try:
        sdev_agg = stdev(aggregate)
    except StatisticsError:
        dev_agg = None
    
    return mean_agg, sdev_agg


def normalized(metric, base_metric):
    try:
        norm = metric/base_metric
    except:
        norm = None
    return norm


def key_epoch_metrics_static(epoch, key_epoch_data,
                             net_reward, step_count, success_value,
                             maintenance_values, maintenance_cost,
                             penalty_values, penalty_cost, penalty_count,
                             epoch_net_rewards, epoch_step_counts, epoch_success_values,
                             epoch_penalty_counts, epoch_penalty_costs, epoch_penalty_values,
                             epoch_maintenance_costs, epoch_maintenance_values,
                             environment_data, learner_data,
                            ):
    
    
    epoch_metrics = {}
    epoch_metrics.update({"environment_data": environment_data})
    epoch_metrics.update({"learner_data": learner_data})
    
    
    penalty_value_mean, penalty_value_sdev = mean_and_sdev(penalty_values)
    penalty_value_mean_normalized = normalized(penalty_value_mean, base_penalty)
    penalty_value_sdev_normalized = normalized(penalty_value_sdev, base_penalty)
    
    maintenance_value_mean, maintenance_value_sdev = mean_and_sdev(maintenance_values)
    maintenance_value_mean_normalized = normalized(maintenance_value_mean, base_maintenance)
    maintenance_value_sdev_normalized = normalized(maintenance_value_sdev, base_maintenance)  
    
    snapshot = {
        "penalty_count": penalty_count,
        "penalty_cost": penalty_cost,
        "penalty_values": penalty_values,
        "penalty_value_mean": penalty_value_mean,
        "penalty_value_sdev": penalty_value_sdev,
        "penalty_value_mean_normalized": penalty_value_mean_normalized,
        "penalty_value_sdev_normalized": penalty_value_sdev_normalized,

        "step_count": step_count,
        "maintenance_cost": maintenance_cost,
        "maintenance_values": maintenance_values,
        "maintenance_value_mean": maintenance_value_mean,
        "maintenance_value_sdev": maintenance_value_sdev,
        "maintenance_value_mean_normalized": maintenance_value_mean_normalized,
        "maintenance_value_sdev_normalized": maintenance_value_sdev_normalized,

        "net_reward": net_reward,
        "success_value": success_value,
        "success_value_normalized": normalized(success_value, base_success),
        }
    
    epoch_metrics.update({"snapshot": snapshot})
    
    
    if epoch >= 100:
        ## last 100 epochs: penalty stats
        penalty_value_mean_last_100, penalty_value_sdev_last_100 = last_100_mean_and_sdev(
            epoch_penalty_values)
        penalty_value_mean_normalized_last_100 = normalized(penalty_value_mean_last_100, base_penalty)
        penalty_value_sdev_normalized_last_100 = normalized(penalty_value_sdev_last_100, base_penalty)

        penalty_cost_mean_last_100, penalty_cost_sdev_last_100 = last_100_mean_and_sdev(
            epoch_penalty_costs)
        penalty_cost_mean_normalized_last_100 = normalized(penalty_cost_mean_last_100, base_penalty)
        penalty_cost_sdev_normalized_last_100 = normalized(penalty_cost_sdev_last_100, base_penalty)

        penalty_count_mean_last_100, penalty_count_sdev_last_100 = last_100_mean_and_sdev(
            epoch_penalty_counts)
        
        penalty_free_density = penalty_free_epoch_density(epoch_penalty_counts)
        
        penalty_ratio_last_100 = sum(epoch_penalty_counts[-100:])/100


        ## last 100 epochs: maintenance stats
        maintenance_value_mean_last_100, maintenance_value_sdev_last_100 = last_100_mean_and_sdev(
            epoch_maintenance_values)
        maintenance_value_mean_normalized_last_100 = normalized(maintenance_value_mean_last_100,
                                                                base_maintenance)
        maintenance_value_sdev_normalized_last_100 = normalized(maintenance_value_sdev_last_100,
                                                                base_maintenance)

        maintenance_cost_mean_last_100, maintenance_cost_sdev_last_100 = last_100_mean_and_sdev(
            epoch_maintenance_costs)
        maintenance_cost_mean_normalized_last_100 = normalized(maintenance_cost_mean_last_100,
                                                               base_maintenance)
        maintenance_cost_sdev_normalized_last_100 = normalized(maintenance_cost_sdev_last_100,
                                                               base_maintenance)

        step_counts_mean_last_100, step_counts_sdev_last_100 = last_100_mean_and_sdev(
            epoch_step_counts)


        ## last 100 epochs: net reward & success stats
        net_reward_mean_last_100, net_reward_sdev_last_100 = last_100_mean_and_sdev(
            epoch_net_rewards)

        success_value_mean_last_100, success_value_sdev_last_100 = last_100_mean_and_sdev(
            epoch_success_values)
        success_value_mean_normalized_last_100 = normalized(success_value_mean_last_100, base_success)
        success_value_sdev_normalized_last_100 = normalized(success_value_sdev_last_100, base_success)


        last_100 = {

            "penalty_count_mean_last_100": penalty_count_mean_last_100,
            "penalty_count_sdev_last_100": penalty_count_sdev_last_100,
            "penalty_free_epoch_density_last_100": penalty_free_density,
            "penalty_ratio_last_100": penalty_ratio_last_100,

            "penalty_value_mean_last_100": penalty_value_mean_last_100,
            "penalty_value_sdev_last_100": penalty_value_sdev_last_100,
            "penalty_value_mean_normalized_last_100": penalty_value_mean_normalized_last_100,
            "penalty_value_sdev_normalized_last_100": penalty_value_sdev_normalized_last_100,

            "penalty_cost_mean_last_100": penalty_cost_mean_last_100,
            "penalty_cost_sdev_last_100": penalty_cost_sdev_last_100,
            "penalty_cost_mean_normalized_last_100": penalty_cost_mean_normalized_last_100,
            "penalty_cost_sdev_normalized_last_100": penalty_cost_sdev_normalized_last_100,

            "step_counts_mean_last_100": step_counts_mean_last_100,
            "step_counts_sdev_last_100": step_counts_sdev_last_100,

            "maintenance_value_mean_last_100": maintenance_value_mean_last_100,
            "maintenance_value_sdev_last_100": maintenance_value_sdev_last_100,
            "maintenance_value_mean_normalized_last_100": maintenance_value_mean_normalized_last_100,
            "maintenance_value_sdev_normalized_last_100": maintenance_value_sdev_normalized_last_100,

            "maintenance_cost_mean_last_100": maintenance_cost_mean_last_100,
            "maintenance_cost_sdev_last_100": maintenance_cost_sdev_last_100,
            "maintenance_cost_mean_normalized_last_100": maintenance_cost_mean_normalized_last_100,
            "maintenance_cost_sdev_normalized_last_100": maintenance_cost_sdev_normalized_last_100,

            "success_value_mean_last_100": success_value_mean_last_100,
            "success_value_sdev_last_100": success_value_sdev_last_100,
            "success_value_mean_normalized_last_100": success_value_mean_normalized_last_100,
            "success_value_sdev_normalized_last_100": success_value_sdev_normalized_last_100,

            "net_reward_mean_last_100": net_reward_mean_last_100,
            "net_reward_sdev_last_100": net_reward_sdev_last_100
        }
        
        epoch_metrics.update({"last_100": last_100})
        
    
    
    key_epoch_data.update({epoch: epoch_metrics})
    return




def update_running_average(counter_list, averages_list, value):
    if (len(counter_list) < 1000):
        average = sum(counter_list)/len(counter_list)
    else:
        average = sum(counter_list[-1000:])/len(counter_list[-1000:])
    averages_list.append(average)
    return
