from random import randrange, uniform
import multiprocessing as mp
from os import cpu_count, getpid
import json
from uuid import uuid1
import pdb


import gym
import numpy as np
from datetime import datetime, timedelta


from environment_settings import base_reinforcement_settings, env_base
from environment_settings import key_epochs, environment_epoch_count
from environment_settings import make_responsiveness, make_harshness, make_chaos_level, chaos_mod
from environment_settings import make_filenames

from data_preprocessing import calculate_feedback, update_metrics_with_feedback
from data_preprocessing import record_penalty_free_epochs
from data_preprocessing import key_epoch_metrics_static as key_epoch_metrics



#from IPython.display import clear_output


# Based on code from https://www.learndatasci.com/tutorials/reinforcement-q-learning-scratch-python-openai-gym/


### environment basics:

flavor = "static"
base_success, base_penalty, base_maintenance = base_reinforcement_settings


TESTING = False

if TESTING:
    processes = 3
    learner_batch_size = 12
else:
    processes = 19
    learner_batch_size = 4000




# learner hyperparameters

def static_alpha():
    alpha = randrange(1.0, 25.0, 1.0)/100
    return alpha

def static_epsilon():
    epsilon = randrange(1.0, 20.0, 1.0)/100
    return epsilon

def static_gamma():
    gamma = randrange(50.0, 99.0, 1.0)/100
    return gamma



"""Training the agent"""


def train_static_learner(chaos=True, test=TESTING):
    
    unique_id = str(uuid1())
    
    start = datetime.now()
    if test:
        print(f"{start}, PID: {getpid()}")
    
    
    ### Environment Data ###
    
    epoch_count = environment_epoch_count
    
    env = gym.make("Taxi-v3").env
    
    if chaos:
        chaos_level = make_chaos_level()
    else:
        chaos_level = 0.0
        
    responsiveness = make_responsiveness()
    harshness = make_harshness()
    
    environment_data = {
        "uuid": unique_id,
        "env_base": env_base,
        "base_success": base_success,
        "base_penalty": base_penalty,
        "base_maintenance": base_maintenance,
        "chaos_level": chaos_level,
        "responsiveness": responsiveness,
        "harshness": harshness,
    }
    
    
    ### Learner Data ###
    
    #Initialize Q table
    q_table = np.zeros([env.observation_space.n, env.action_space.n])
        
    
    
    learner_data = {
        "uuid": unique_id,
        "flavor": flavor, 
    }
    
    alpha = static_alpha()
    gamma = static_gamma()
    epsilon = static_epsilon()
    
    hparam_data = {
        "alpha": alpha,
        "initial_alpha": alpha,
        "final_alpha": alpha,
        "alpha_slope": 0.0,
        
        "epsilon": epsilon,
        "initial_epsilon": epsilon,
        "final_epsilon": epsilon,
        "epsilon_slope": 0.0,
        
        "gamma": gamma,
        "initial_gamma": gamma,
        "final_gamma": gamma,
        "gamma_slope": 0.0,
    }
    
    learner_data.update({"hparam_data": hparam_data})
    
    
    
    ### Output Data ###
    
    epoch_step_counts = []
    epoch_maintenance_values = []
    epoch_maintenance_costs = []
    
    epoch_penalty_counts = []
    epoch_penalty_values = []
    epoch_penalty_costs = []
    
    epoch_success_values = []
    epoch_net_rewards = []
    
    penalty_free_epoch_runs = {
        1: None,
        3: None,
        5: None,
        10: None,
        15: None,
        20: None,
        25: None,
        30: None,
        40: None,
        50: None,
        60: None,
        70: None,
        80: None,
        90: None,
        100: None
    }
    
    key_epoch_data = {}
    
    output_data = {
        "epoch_step_counts": epoch_step_counts,
        "epoch_maintenance_costs": epoch_maintenance_costs,
    
        "epoch_penalty_counts": epoch_penalty_counts,
        "epoch_penalty_costs": epoch_penalty_costs,
    
        "epoch_success_values": epoch_success_values,
        "epoch_net_rewards": epoch_net_rewards,
        
        "penalty_free_epoch_runs": penalty_free_epoch_runs,
    }
                               
                              
    
    metadata = {
        "uuid": unique_id,
        "environment_data": environment_data,
        "learner_data": learner_data,
        "output_data": output_data,
        "key_epoch_data": key_epoch_data,
        "training_time": None,
        }
    
    #print("Initialization Complete")
    #pdb.set_trace()
    
    for epoch in range(1, (epoch_count + 1)):
        
        #if epoch == 1:
        #    print("Training new sim.")
            
        state = env.reset()
        
        epoch_data = {
            "step_count": 0,
            "net_reward": 0,
            "penalty_count": 0,
            "penalty_values": [],
            "penalty_cost": 0,
            "maintenance_values": [],
            "maintenance_cost": 0,
            "success_value": 0
        }
        
        
        
        done = False
        
        while not done:
            
            if uniform(0, 1) < epsilon:
                action = env.action_space.sample() # Explore action space
            else:
                action = np.argmax(q_table[state]) # Exploit learned values

            next_state, reward, done, info = env.step(action)
            

            feedback = calculate_feedback(reward, 
                                          chaos_level, 
                                          harshness, 
                                          responsiveness)
            
            update_metrics_with_feedback(feedback, epoch_data)
            
            custom_reward = feedback["custom_reward"]
            
            
            #update q_table and step env
            old_value = q_table[state, action]
            next_max = np.max(q_table[next_state])
            
            new_value = (1 - alpha) * old_value + alpha * (custom_reward + gamma * next_max)
            q_table[state, action] = new_value
            
            state = next_state
            
 
            
        # update metrics and hparams for next epoch

        epoch_net_rewards.append(epoch_data["net_reward"])
        epoch_step_counts.append(epoch_data["step_count"])
        epoch_success_values.append(epoch_data["success_value"])
        
        epoch_maintenance_values.append(epoch_data["maintenance_values"])
        epoch_maintenance_costs.append(epoch_data["maintenance_cost"])
        
        epoch_penalty_counts.append(epoch_data["penalty_count"])
        epoch_penalty_values.append(epoch_data["penalty_values"])
        epoch_penalty_costs.append(epoch_data["penalty_cost"])
        
        record_penalty_free_epochs(epoch, 
                                   epoch_penalty_counts, 
                                   penalty_free_epoch_runs)
        
        if epoch in key_epochs:
            key_epoch_metrics(epoch, key_epoch_data,
                              epoch_data["net_reward"], epoch_data["step_count"],
                              epoch_data["success_value"],
                              epoch_data["maintenance_values"], epoch_data["maintenance_cost"],
                              epoch_data["penalty_values"], epoch_data["penalty_cost"], 
                              epoch_data["penalty_count"],
                              epoch_net_rewards, epoch_step_counts, epoch_success_values,
                              epoch_penalty_counts, epoch_penalty_costs, epoch_penalty_values,
                              epoch_maintenance_costs, epoch_maintenance_values,
                              environment_data, learner_data)

        
        latest = datetime.now()
        training_time = latest-start
        metadata["training_time"] = str(training_time)

        if training_time > timedelta(minutes=10):
            print("Aborted {} learner at {}.".format(flavor, latest))
            return None, q_table
            
    return metadata, q_table




def static_sim_differential_env_orthogonal(c_arg):
    
    metadata, q_table = train_static_learner(chaos=c_arg)
    return_value = int(bool(metadata))

    if metadata:
        return_value = 1
        
        unique_id = metadata["uuid"]
        
        m_file, q_file = make_filenames(flavor, unique_id)

        with open(m_file, "w") as dest:
            json.dump(metadata, dest)
            
        table_dict = {"uuid": unique_id,
                      "q_table": str(q_table)}
        
        with open(q_file, "w") as dest:
            json.dump(table_dict, dest)
            
    return return_value



results = []

def update_results(result):
    if result == 1:
        results.append(result)
        print("{} {} {} sims trained and saved at {}, PID: {}".format(sum(results), 
                                                                      flavor, 
                                                                      env_base, 
                                                                      datetime.now(),
                                                                      getpid()))
        return


def generate_args(batch_size):
    for b in range(batch_size):
        a = bool(b%5==0)
        yield a


if __name__ == "__main__":
    start = datetime.now()
    #cpus = cpu_count() - 1
    procs = processes
    N = learner_batch_size
    
    #arg_gen = generate_args(N)
    arg_gen = (False for i in range(N))
    
    training_func = static_sim_differential_env_orthogonal
    
    
    print("Starting {} {} {} sims in {} processes.".format(N, flavor, env_base, procs))
    
    with mp.Pool(processes=procs) as pool:
        
        for c_arg in arg_gen:

            pool.apply_async(training_func, 
                             args=(c_arg,), 
                             callback=update_results)

        pool.close()
        print("Pool Closing")
        pool.join()
        print("Pool joining.")

        
    print("Finishing {} sims".format(flavor))
    end = datetime.now()
    elapsed_time = end-start
    print("{} {} {} sims run in {}.".format(N, flavor, env_base, str(elapsed_time)))
    average_time = (elapsed_time*procs)/len(results)
    print(f"Average training time: {str(average_time)} per sim.")
    
    

"""

if __name__ == "__main__":
    
    start = datetime.now()
    #print(start)
    print("Testing making 1 {} {} sim".format(flavor, env_base))
    result = static_sim_differential_env_orthogonal(c_arg=True)
    end = datetime.now()
    print("Elapsed time: {}".format(end-start))


"""