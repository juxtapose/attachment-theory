from random import gauss, seed, randrange
import random
import multiprocessing as mp
from os import cpu_count
import json
from math import sqrt, fabs


import pandas as pd
import gym
import numpy as np
from datetime import datetime, timedelta



# Based on code from https://www.learndatasci.com/tutorials/reinforcement-q-learning-scratch-python-openai-gym/


"""Training the agent"""


# learner hyperparameters

def relative_differential():
    sign = random.choice([-1.0, 1.0])
    ratio = randrange(1.0, 30.0)/100.
    #multiplier = 1+(ratio*sign)
    return ratio, sign


def dynamic_alpha():
    
    alpha_metaparams = {
        "initial": randrange(5.0, 25.0, 1.0)/100.,
        "minimum": 0.01,
        "maximum": 0.3,
        "steps_differential": relative_differential(),
        "reward_differential": relative_differential(),
        "penalties_differential": relative_differential(),
    }
    return alpha_metaparams

def dynamic_gamma():
    
    gamma_metaparams = {
        "initial": randrange(60.0, 95.0, 1.0)/100.,
        "minimum": 0.5,
        "maximum": 0.99,
        "steps_differential": relative_differential(),
        "reward_differential": relative_differential(),
        "penalties_differential": relative_differential(),
    }
    return gamma_metaparams

def dynamic_epsilon():
    
    epsilon_metaparams = {
        "initial": randrange(5.0, 40.0, 1.0)/100.,
        "minimum": 0.01,
        "maximum": 0.5,
        "steps_differential": relative_differential(),
        "reward_differential": relative_differential(),
        "penalties_differential": relative_differential(),
    }
    return epsilon_metaparams



def update_running_average(counter_list, averages_list, learning_interval):
    if (len(counter_list) < 1000):
        average = sum(counter_list)/len(counter_list)
    else:
        average = sum(counter_list[-1000:])/len(counter_list[-1000:])
    averages_list.append(average)
    return



def metric_relative_differential(averages_list, learning_interval):
    latest_running_avg = averages_list[-1]
    prev_running_avg = averages_list[-learning_interval]
    denom = (latest_running_avg+prev_running_avg)/2.0
    try:
        if denom == 0:
            return 0, 1
        else:
            relative_differential = (latest_running_avg-prev_running_avg)/denom
            sign_rel_diff = relative_differential/fabs(relative_differential)
            return relative_differential, sign_rel_diff
    except ZeroDivisionError:
        return 0.0, 1



def update_hparam(hparam, hparam_metaparams,
                  rewards_r_avg,
                  penalties_r_avg,
                  steps_r_avg,
                  learning_interval):
 
    rewards_rel_diff, rewards_diff_sign = metric_relative_differential(rewards_r_avg, learning_interval)
    penalties_rel_diff, penalties_diff_sign = metric_relative_differential(penalties_r_avg, learning_interval)
    steps_rel_diff, steps_diff_sign = metric_relative_differential(steps_r_avg, learning_interval)

    h_rewards_ratio = hparam_metaparams['reward_differential'][0]
    h_rewards_sign = hparam_metaparams['reward_differential'][1]

    h_penalties_ratio = hparam_metaparams['penalties_differential'][0]
    h_penalties_sign = hparam_metaparams['penalties_differential'][1]

    h_steps_ratio = hparam_metaparams['steps_differential'][0]
    h_steps_sign = hparam_metaparams['steps_differential'][1]
    
    del_sq_rewards = (h_rewards_ratio*rewards_rel_diff)**2
    del_sq_penalties = (h_penalties_ratio*penalties_rel_diff)**2
    del_sq_steps = (h_steps_ratio*steps_rel_diff)**2
    
    
    try:
        sign_del = ((h_steps_sign * steps_diff_sign * del_sq_steps + 
                     h_rewards_sign * rewards_diff_sign * del_sq_rewards + 
                     h_penalties_sign * penalties_diff_sign * del_sq_penalties)/
                     fabs(h_steps_sign * steps_diff_sign * del_sq_steps + 
                          h_rewards_sign * rewards_diff_sign * del_sq_rewards + 
                          h_penalties_sign * penalties_diff_sign * del_sq_penalties))

        mag_del = sqrt(del_sq_rewards + del_sq_penalties + del_sq_steps)
        
        del_ = sign_del*mag_del
        
    except ZeroDivisionError: # it hypothetically could happen
        return hparam
    
    
    calculated = hparam * (1 + del_)

    if calculated < hparam_metaparams['minimum']:
        return hparam_metaparams['minimum']
    elif calculated > hparam_metaparams['maximum']:
        return hparam_metaparams['maximum']
    else:
        return calculated
    
    
    
def convergence_check(running_average_list, convergence_interval=100):
    if metric_relative_differential(running_average_list, convergence_interval) < 0.01:
        return True
    return False




# environmental reward modifiers

def make_chaos_level():
    chaos_level = randrange(start=0.0, stop=100.0, step=1.0)/100
    return chaos_level


def chaos_mod(chaos_level):
    if (random.uniform(0,1) < chaos_level):
        mod = gauss(0.0, chaos_level)
    else:
        mod = 0
    return mod


def calculate_reward(base_reward, chaos_level):
    percent_diff = chaos_mod(chaos_level)
    adjusted_reward = base_reward*(1+percent_diff)
    return adjusted_reward


def train_dynamic_learner(chaos=True):
    
    start = dt.now()
    
    epoch_count = 60000
    learning_interval = random.choice([250, 500, 1000])
    
    env = gym.make("Taxi-v3").env
    
    #Initialize Q table
    q_table = np.zeros([env.observation_space.n, env.action_space.n])
    
    alpha_metaparams = dynamic_alpha()
    gamma_metaparams = dynamic_gamma()
    epsilon_metaparams = dynamic_epsilon()
    
    alpha = alpha_metaparams["initial"]
    gamma = gamma_metaparams["initial"]
    epsilon = epsilon_metaparams["initial"]
        
    alpha_by_epoch = []
    gamma_by_epoch = []
    epsilon_by_epoch = []
    
    if chaos:
        chaos_level = make_chaos_level()
    else:
        chaos_level = 0.0
        
    
    
    # For plotting metrics
    epoch_step_counts = []
    base_reward_totals = []
    custom_reward_totals = []
    penalty_counts = []
    
    epoch_step_count_running_average = []
    base_reward_running_average = []
    custom_reward_running_average = []
    penalty_running_average = []
    
    
    metadata = {
        "alpha_metaparams": alpha_metaparams,
        "gamma_metaparams": gamma_metaparams,
        "epsilon_metaparams": epsilon_metaparams,
        "alpha_by_epoch": alpha_by_epoch,
        "gamma_by_epoch": gamma_by_epoch,
        "epsilon_by_epoch": epsilon_by_epoch,
        "chaos_level": chaos_level,
        "epoch_step_counts": epoch_step_counts,
        "base_reward_totals": base_reward_totals,
        "custom_reward_totals": custom_reward_totals,
        "penalty_counts": penalty_counts,
        "learning_interval": learning_interval
        }
    
    
    for i in range(1, (epoch_count + 1)):
        state = env.reset()

        epochs, penalties, reward = 0, 0, 0
        
        step_count = 0
        net_base_reward = 0
        net_custom_reward = 0
        
        alpha_by_epoch.append(alpha)
        gamma_by_epoch.append(gamma)
        epsilon_by_epoch.append(epsilon)
        
        done = False

        while not done:
            
            if random.uniform(0, 1) < epsilon:
                action = env.action_space.sample() # Explore action space
            else:
                action = np.argmax(q_table[state]) # Exploit learned values

            next_state, reward, done, info = env.step(action)
            

            old_value = q_table[state, action]
            next_max = np.max(q_table[next_state])
            
            custom_reward = calculate_reward(reward, chaos_level)

            new_value = (1 - alpha) * old_value + alpha * (custom_reward + gamma * next_max)

            q_table[state, action] = new_value


            if reward == -10:
                penalties += 1

            net_base_reward += reward
            net_custom_reward += custom_reward
            step_count += 1
            
            state = next_state
            
            
        # update params and hparams for next epoch
        base_reward_totals.append(net_base_reward)
        custom_reward_totals.append(net_custom_reward)
        epoch_step_counts.append(step_count)
        penalty_counts.append(penalties)
        
        update_running_average(base_reward_totals, base_reward_running_average, learning_interval)
        update_running_average(custom_reward_totals, custom_reward_running_average, learning_interval)
        update_running_average(epoch_step_counts, epoch_step_count_running_average, learning_interval)
        update_running_average(penalty_counts, penalty_running_average, learning_interval)
        
        
        latest = dt.now()
        training_time = latest-start
        metadata["training_time"] = str(training_time)
        
        if (training_time > timedelta(minutes=10)):
            return None

        if i%learning_interval==0:
            alpha = update_hparam(alpha, alpha_metaparams, 
                                  custom_reward_running_average,
                                  penalty_running_average,
                                  epoch_step_count_running_average,
                                  learning_interval)

            gamma = update_hparam(gamma, gamma_metaparams, 
                                  custom_reward_running_average,
                                  penalty_running_average,
                                  epoch_step_count_running_average,
                                  learning_interval)
            
            epsilon = update_hparam(epsilon, epsilon_metaparams, 
                                    custom_reward_running_average,
                                    penalty_running_average,
                                    epoch_step_count_running_average,
                                    learning_interval) 
            
    
         
            
    return metadata


    
def dynamic_sim_no_chaos():
    result = train_dynamic_learner(chaos=False)
    filename = "./simulation_data/dynamic_learners_no_chaos.json"
    return result, filename


def dynamic_sim_with_chaos():
    result = train_dynamic_learner(chaos=True)
    filename = "./simulation_data/dynamic_learners_with_chaos.json"
    return result, filename

from datetime import datetime as dt
print(dt.now())



if __name__ == "__main__":
    start = datetime.now()
    #cpus = cpu_count() - 1
    procs = 3
    N = 350
    
    print(start)
    print("Starting {} dynamic sims in {} processes.".format(N, procs))
    with mp.Pool(processes=procs) as pool:
        
        #i = 0
        total = 0
        for i in range(N):
            
            result, filename = pool.apply_async(dynamic_sim_with_chaos).get()
            if result:
                with open(filename, "a") as dest:
                    dest.write(json.dumps(result) + '\n')
                    total += 1
        
            if i >= 250:
                # make 100 with 0 chaos as baseline. Don't need giant control group!!
                result_2, filename_2 = pool.apply_async(dynamic_sim_no_chaos).get() 
                if result_2:
                    with open(filename_2, "a") as dest:
                        dest.write(json.dumps(result) + '\n')
                        total += 1
                        
            print("{} sims saved at {}".format(total, dt.now()))

        
    print("Finishing sims")
    end = datetime.now()
    print("{} sims run in {}.".format(total, str(end-start)))
    
from datetime import datetime as dt
start = dt.now()
print("Start: " + str(start))