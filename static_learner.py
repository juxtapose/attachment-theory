
from random import gauss, seed, randrange
import random
import multiprocessing as mp
from os import cpu_count
import json

import pandas as pd
import gym
import numpy as np
from datetime import datetime


# Based on code from https://www.learndatasci.com/tutorials/reinforcement-q-learning-scratch-python-openai-gym/



# Hyperparameters

def static_alpha():
    alpha = randrange(1.0, 25.0, 1.0)/100
    return alpha

def static_epsilon():
    epsilon = randrange(1.0, 20.0, 1.0)/100
    return epsilon

def static_gamma():
    gamma = randrange(50.0, 99.0, 1.0)/100
    return gamma



# environmental reward modifiers

def make_chaos_level():
    chaos_level = randrange(start=0.0, stop=100.0, step=1.0)/100
    return chaos_level


def chaos_mod(chaos_level):
    if (random.uniform(0,1) < chaos_level):
        mod = gauss(0.0, chaos_level)
    else:
        mod = 0
    return mod


def calculate_reward(base_reward, chaos_level):
    percent_diff = chaos_mod(chaos_level)
    adjusted_reward = base_reward*(1+percent_diff)
    return adjusted_reward


# update functions

def update_running_average(counter_list, averages_list, value):
    if (len(counter_list) < 1000):
        average = sum(counter_list)/len(counter_list)
    else:
        average = sum(counter_list[-1000:])/len(counter_list[-1000:])
    averages_list.append(average)
    return



def train_static_learner(chaos=True):
    
    epoch_count = 60000
    
    env = gym.make("Taxi-v3").env
    
    #Initialize Q table
    q_table = np.zeros([env.observation_space.n, env.action_space.n])
    
    
    alpha = static_alpha()
    gamma = static_gamma()
    epsilon = static_epsilon()
    
    
    if chaos:
        chaos_level = make_chaos_level()
    else:
        chaos_level = 0.0
        
    
    # For plotting metrics
    epoch_step_counts = []
    base_reward_totals = []
    custom_reward_totals = []
    penalty_counts = []
    
    #running_average_step_counts = []
    #running_average_base_rewards = []
    #running_average_custom_rewards = []
    #running_average_penalty_counts = []
    
    
    metadata = {
        "initial_alpha": alpha,
        "final_alpha": alpha,
        "alpha_slope": 0.0,
        "initial_epsilon": epsilon,
        "final_epsilon": epsilon,
        "epsilon_slope": 0.0,
        "initial_gamma": gamma,
        "final_gamma": gamma,
        "gamma_slope": 0.0,
        "chaos_level": chaos_level,
        "base_reward_totals": base_reward_totals,
        "custom_reward_totals": custom_reward_totals,
        "epoch_step_counts": epoch_step_counts,
        "penalty_counts": penalty_counts,
        #"running_average_penalty_counts": running_average_penalty_counts,
        #"running_average_base_rewards": running_average_base_rewards,
        #"running_average_custom_rewards": running_average_custom_rewards,
        #"running_average_step_counts": running_average_step_counts
    }
    
    
    
    for i in range(1, (epoch_count + 1)):
        state = env.reset()

        epochs, penalties, reward = 0, 0, 0
        
        step_count = 0
        net_base_reward = 0
        net_custom_reward = 0
        
        
        done = False

        while not done:
         
            if random.uniform(0, 1) < epsilon:
                action = env.action_space.sample() # Explore action space
            else:
                action = np.argmax(q_table[state]) # Exploit learned values

            next_state, reward, done, info = env.step(action)
            
            old_value = q_table[state, action]
            next_max = np.max(q_table[next_state])

            custom_reward = calculate_reward(reward, chaos_level)
            
            new_value = (1 - alpha) * old_value + alpha * (custom_reward + gamma * next_max)
            q_table[state, action] = new_value

            if reward == -10:
                penalties += 1
                
            net_base_reward += reward
            net_custom_reward += custom_reward
            step_count += 1
            
            state = next_state
            
        # update stats for epoch
        base_reward_totals.append(net_base_reward)
        custom_reward_totals.append(net_custom_reward)
        epoch_step_counts.append(step_count)
        penalty_counts.append(penalties)

        #update_running_average(base_reward_totals, running_average_base_rewards, net_base_reward)
        #update_running_average(custom_reward_totals, running_average_custom_rewards, net_custom_reward)
        #update_running_average(epoch_step_counts, running_average_step_counts, step_count)
        #update_running_average(penalty_counts, running_average_penalty_counts, penalties)
        
        # update params and hparams for next epoch
        
    #print("Training finished.\n")
    return metadata


    
def static_sim_no_chaos():
    result = train_static_learner(chaos=True)
    filename = "./simulation_data/static_learners_no_chaos_BETTER.json"
    return result, filename


def static_sim_with_chaos():
    result = train_static_learner(chaos=False)
    filename = "./simulation_data/static_learners_with_chaos_BETTER.json"
    return result, filename




if __name__ == "__main__":
    start = datetime.now()
    #procs = cpu_count()-2
    procs = 4
    N = 341
    
    print(start)
    
  
    print("Starting {} static sims in {} processes.".format(N, procs))

    with mp.Pool(processes=procs) as pool:
        
        for i in range(N):
        
            result, filename = pool.apply_async(static_sim_no_chaos).get() 

            with open(filename, "a") as dest:
                dest.write(json.dumps(result) + '\n')
                
                
            result, filename = pool.apply_async(static_sim_with_chaos).get()
            
            with open(filename, "a") as dest:
                dest.write(json.dumps(result) + '\n')
                
                """
    
    print("Starting 2*{} static sims in series.".format(N))
    
    for i in range(N):
        
            result, filename = static_sim_no_chaos()

            with open(filename, "a") as dest:
                dest.write(json.dumps(result) + '\n')
                
                
            result2, filename2 = static_sim_with_chaos()
            
            with open(filename2, "a") as dest:
                dest.write(json.dumps(result2) + '\n')
                  """

        
    print("Finishing sims")
    end = datetime.now()
    print("{} sims run in {}.".format(str(2*N), str(end-start)))