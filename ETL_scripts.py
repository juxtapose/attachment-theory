import math
import json
from datetime import datetime as dt

import pandas as pd
import numpy
from statistics import stdev, pstdev, pvariance, variance, mean

#import pdb



def load_json_from_file(filename):
    loaded = [json.loads(l) for l in file.readlines()]
    return loaded



### Basic functions for loading and xforming
    
    
def metric_avg_at_mark(metric, mark):
    #pdb.set_trace()
    if not mark:
        return None
    chunk = metric[mark-100:mark+100]
    avg = mean(chunk)
    return avg
    
def metric_sdev_at_mark(metric, mark):
    #pdb.set_trace()
    if not mark:
        return None
    chunk = metric[mark-100:mark+100]
    std = stdev(chunk)
    return std

def metric_slope_at_mark(metric, mark):
    #pdb.set_trace()
    if not mark:
        return None
    pre_mark = mark-100
    post_mark = mark+100
    pre_avg = metric_avg_at_mark(metric, pre_mark)
    post_avg = metric_avg_at_mark(metric, post_mark)
    slope = (post_avg-pre_avg)/200
    return slope
    
    
def rewards_convergence_occurs(rewards_data):
    #pdb.set_trace()
    # if condition: return value, epoch count, else return None
    for mark in range(500, len(rewards_data), 500):
        chunk = rewards_data[(mark-500):(mark)]
        avg = mean(chunk)
        std = stdev(chunk)
        if (std < avg):
            return mark
    return None

def avg_rewards_become_positive(rewards_data):
    for mark in range(500, len(rewards_data), 500):
        chunk = rewards_data[(mark-500):(mark)]
        avg = mean(chunk)
        if avg>0:
            return mark
        return None


def penalty_convergence_occurs(penalty_counts):
    for mark in range(500, len(penalty_counts), 500):
        chunk = penalty_counts[(mark-500):(mark)]
        avg = mean(chunk)
        std = stdev(chunk)
        if sum(chunk)<=5:
            return mark
        #elif (std < avg):
        #    return mark
    return None


def steps_convergence_occurs(step_data):
    #pdb.set_trace()
    # if condition: return epoch count, else return None
    for mark in range(500, len(step_data), 500):
        chunk = step_data[(mark-500):(mark)]
        maximum = max(chunk)
        avg = mean(chunk)
        if (maximum < 2*avg):
            return mark
    return None
        

## def convergence:
## running average reaches +- 10% of final average (might try 5% and see what happens)
## if no convergence:
## running average during training never meets that threshold before actual last epochs



def etl_metric_features(model):

    # base reward stats
    epochs_to_base_reward_convergence = rewards_convergence_occurs(model["base_reward_totals"])
    base_reward_dict = {
        "average_base_reward_1000_epochs": metric_avg_at_mark(model["base_reward_totals"],1000),
        "average_base_reward_5000_epochs": metric_avg_at_mark(model["base_reward_totals"],5000),
        "average_base_reward_10000_epochs": metric_avg_at_mark(model["base_reward_totals"],10000),
        "average_base_reward_30000_epochs": metric_avg_at_mark(model["base_reward_totals"],30000),
        "average_base_reward_50000_epochs": metric_avg_at_mark(model["base_reward_totals"],50000),
        "average_base_reward_60000_epochs": metric_avg_at_mark(model["base_reward_totals"],60000),


        #"epochs_to_base_reward_convergence": epochs_to_base_reward_convergence,
        #"average_base_reward_at_convergence": metric_avg_at_mark(model["base_reward_totals"], 
        #                                                         epochs_to_base_reward_convergence),
        "epochs_to_positive_base_rewards": avg_rewards_become_positive(model["base_reward_totals"]),
        "cumulative_base_rewards_5k_to_30k": sum(model["base_reward_totals"][5000:30000]),
        "cumulative_base_rewards_30k_to_60k": sum(model["base_reward_totals"][30000:60000]),
        #"post_convergence_base_reward_variance": 0.
        }
    


    # adjusted reward stats
    epochs_to_adjusted_reward_convergence = rewards_convergence_occurs(model["custom_reward_totals"])
    adjusted_reward_dict = {
        "average_adjusted_reward_1000_epochs": metric_avg_at_mark(model["custom_reward_totals"],1000),
        "average_adjusted_reward_5000_epochs": metric_avg_at_mark(model["custom_reward_totals"],5000),
        "average_adjusted_reward_10000_epochs": metric_avg_at_mark(model["custom_reward_totals"],10000),
        "average_adjusted_reward_30000_epochs": metric_avg_at_mark(model["custom_reward_totals"],30000),
        "average_adjusted_reward_50000_epochs": metric_avg_at_mark(model["custom_reward_totals"],50000),
        "average_adjusted_reward_60000_epochs": metric_avg_at_mark(model["custom_reward_totals"],60000),



        #"epochs_to_adjusted_reward_convergence": epochs_to_adjusted_reward_convergence,
        #"average_adjusted_reward_at_convergence": metric_avg_at_mark(model["custom_reward_totals"], 
        #                                                             epochs_to_adjusted_reward_convergence),
        "epochs_to_positive_adjusted_rewards": avg_rewards_become_positive(model["custom_reward_totals"]),
        "cumulative_adjusted_rewards_5k_to_30k": sum(model["custom_reward_totals"][5000:30000]),
        "cumulative_adjusted_rewards_30k_to_60k": sum(model["custom_reward_totals"][30000:60000]),
        #"post_convergence_adusted_reward_variance": 0.
    }


    # penalty stats
    epochs_to_penalty_convergence = penalty_convergence_occurs(model["penalty_counts"])
    penalty_dict = {
        "average_penalties_1000_epochs": metric_avg_at_mark(model["penalty_counts"],1000),
        "average_penalties_5000_epochs": metric_avg_at_mark(model["penalty_counts"],5000),
        "average_penalties_10000_epochs": metric_avg_at_mark(model["penalty_counts"],10000),
        "average_penalties_30000_epochs": metric_avg_at_mark(model["penalty_counts"],30000),
        "average_penalties_50000_epochs": metric_avg_at_mark(model["penalty_counts"],50000),
        "average_penalties_60000_epochs": metric_avg_at_mark(model["penalty_counts"],60000),


        #"epochs_to_penalty_convergence": epochs_to_penalty_convergence,
        #"average_penalty_at_convergence": metric_avg_at_mark(model["penalty_counts"], 
        #                                                     epochs_to_penalty_convergence),
        "cumulative_penalties_5k_to_30k": sum(model["penalty_counts"][5000:30000]),
        "cumulative_penalties_30k_to_60k": sum(model["penalty_counts"][30000:60000]),
        #"post_convergence_reward_variance": 0.
    }


    # epoch step count stats
    epochs_to_steps_convergence = steps_convergence_occurs(model["epoch_step_counts"])
    steps_dict = {
        "average_steps_1000_epochs": metric_avg_at_mark(model["epoch_step_counts"],1000),
        "average_steps_5000_epochs": metric_avg_at_mark(model["epoch_step_counts"],5000),
        "average_steps_10000_epochs": metric_avg_at_mark(model["epoch_step_counts"],10000),
        "average_steps_30000_epochs": metric_avg_at_mark(model["epoch_step_counts"],30000),
        "average_steps_50000_epochs": metric_avg_at_mark(model["epoch_step_counts"],50000),
        "average_steps_60000_epochs": metric_avg_at_mark(model["epoch_step_counts"],60000),


        #"epochs_to_steps_convergence": epochs_to_steps_convergence,
        #"average_steps_at_convergence": metric_avg_at_mark(model["epoch_step_counts"],
        #                                                   epochs_to_steps_convergence)
        #"post_convergence_steps_variance": 0.
    }
    
    return base_reward_dict, adjusted_reward_dict, penalty_dict, steps_dict



#### LOAD ENVIRONMENT FEATURES ####


def etl_static_environment_features(model):
    
    env_stats = {
        "chaos_level": model["chaos_level"],
        "harshness": None,
        "responsiveness": None,
        "learning_interval": None,
    }
    return env_stats




def etl_linear_environment_features(model):
    
    env_stats = {
        "chaos_level": model["chaos_level"],
        "harshness": None,
        "responsiveness": None,
        "learning_interval": None,
    }
    return env_stats




def etl_dynamic_environment_features(model):
    env_stats = {
        "chaos_level": model["chaos_level"],
        "harshness": None,
        "responsiveness": None,
        "learning_interval": model["learning_interval"],
    }
    return env_stats


def etl_dynamic_diff_environment_features(model):
    env_stats = {
        "chaos_level": model["chaos_level"],
        "harshness": model["harshness"],
        "responsiveness": model["responsiveness"],
        "learning_interval": model["learning_interval"],
    }
    return env_stats



#### LOAD HYPERPARAMETER FEATURES ####


def get_ratios(metric1, metric2):
        try:
            ratio = metric1/metric2
        except ZeroDivisionError:
            ratio = None
        return ratio




def etl_static_hparam_features(model):
    
    epochs_to_base_reward_convergence = rewards_convergence_occurs(model["base_reward_totals"])
    epochs_to_adjusted_reward_convergence = rewards_convergence_occurs(model["custom_reward_totals"])
    #pdb.set_trace()
    epochs_to_penalty_convergence = penalty_convergence_occurs(model["penalty_counts"])
    epochs_to_steps_convergence = steps_convergence_occurs(model["epoch_step_counts"])

        
    # alpha features and alpha diffs
    alpha_dict = {
        "initial_alpha": model["initial_alpha"],
        "average_alpha_1000_epochs": model["initial_alpha"],
        "average_alpha_5000_epochs": model["initial_alpha"],
        "average_alpha_10000_epochs": model["initial_alpha"],
        "average_alpha_30000_epochs": model["initial_alpha"],
        "average_alpha_50000_epochs": model["initial_alpha"],
        "average_alpha_at_base_rewards_convergence": model["initial_alpha"],
        "average_alpha_at_adjusted_rewards_convergence": model["initial_alpha"],
        "average_alpha_at_penalty_convergence": model["initial_alpha"],
        "average_alpha_at_steps_convergence": model["initial_alpha"],
        "average_alpha_slope_1000_epochs": 0.,
        "average_alpha_slope_5000_epochs": 0.,
        "average_alpha_slope_10000_epochs": 0.,
        "average_alpha_slope_30000_epochs": 0.,
        "average_alpha_slope_50000_epochs": 0.,
        #"average_alpha_slope_at_base_rewards_convergence": 0.,
        #"average_alpha_slope_at_adjusted_rewards_convergence": 0.,
        #"average_alpha_slope_at_penalty_convergence": 0.,
        #"average_alpha_slope_at_steps_convergence": 0.,
        }


    # gamma features and gamma diffs
    gamma_dict = {
        "initial_gamma": model["initial_gamma"],
        "average_gamma_1000_epochs": model["initial_gamma"],
        "average_gamma_5000_epochs": model["initial_gamma"],
        "average_gamma_10000_epochs": model["initial_gamma"],
        "average_gamma_30000_epochs": model["initial_gamma"],
        "average_gamma_50000_epochs": model["initial_gamma"],
        "average_gamma_at_base_rewards_convergence": model["initial_gamma"],
        "average_gamma_at_adjusted_rewards_convergence": model["initial_gamma"],
        "average_gamma_at_penalty_convergence": model["initial_gamma"],
        "average_gamma_at_steps_convergence": model["initial_gamma"],
        
        "average_gamma_slope_1000_epochs": 0.,
        "average_gamma_slope_5000_epochs": 0.,
        "average_gamma_slope_10000_epochs": 0.,
        "average_gamma_slope_30000_epochs": 0.,
        "average_gamma_slope_50000_epochs": 0.,
        #"average_gamma_slope_at_base_rewards_convergence": 0.,
        #"average_gamma_slope_at_adjusted_rewards_convergence": 0.,
        #"average_gamma_slope_at_penalty_convergence": 0.,
        #"average_gamma_slope_at_steps_convergence": 0.
        }


    # epsilon features and epsilon diffs
    epsilon_dict = {
    "initial_epsilon": model["initial_epsilon"],
    "average_epsilon_1000_epochs": model["initial_epsilon"],
    "average_epsilon_5000_epochs": model["initial_epsilon"],
    "average_epsilon_10000_epochs": model["initial_epsilon"],
    "average_epsilon_30000_epochs": model["initial_epsilon"],
    "average_epsilon_50000_epochs": model["initial_epsilon"],
    #"average_epsilon_at_base_rewards_convergence": model["initial_epsilon"],
    #"average_epsilon_at_adjusted_rewards_convergence": model["initial_epsilon"],
    #"average_epsilon_at_penalty_convergence": model["initial_epsilon"],
    #"average_epsilon_at_steps_convergence": model["initial_epsilon"],
        
    "average_epsilon_slope_1000_epochs": 0.,
    "average_epsilon_slope_5000_epochs": 0.,
    "average_epsilon_slope_10000_epochs": 0.,
    "average_epsilon_slope_30000_epochs": 0.,
    "average_epsilon_slope_50000_epochs": 0.,
    #"average_epsilon_slope_at_base_rewards_convergence": 0.,
    #"average_epsilon_slope_at_adjusted_rewards_convergence": 0.,
    #"average_epsilon_slope_at_penalty_convergence": 0.,
    #"average_epsilon_slope_at_steps_convergence": 0.
    }
    
    ratio_dict = {
        "ratio_alpha_to_gamma_initial": get_ratios(alpha_dict["initial_alpha"],gamma_dict["initial_gamma"]),
        "ratio_gamma_to_epsilon_initial": get_ratios(gamma_dict["initial_gamma"],epsilon_dict["initial_epsilon"]),
        "ratio_epsilon_to_alpha_initial": get_ratios(epsilon_dict["initial_epsilon"],alpha_dict["initial_alpha"]),
        "ratio_alpha_to_gamma_5k": get_ratios(alpha_dict["average_alpha_5000_epochs"],gamma_dict["average_gamma_5000_epochs"]),
        "ratio_gamma_to_epsilon_5k": get_ratios(gamma_dict["average_gamma_5000_epochs"],epsilon_dict["average_epsilon_5000_epochs"]),
        "ratio_epsilon_to_alpha_5k": get_ratios(epsilon_dict["average_epsilon_5000_epochs"],alpha_dict["average_alpha_5000_epochs"]),
        "ratio_alpha_to_gamma_10k": get_ratios(alpha_dict["average_alpha_10000_epochs"],gamma_dict["average_gamma_10000_epochs"]),
        "ratio_gamma_to_epsilon_10k": get_ratios(gamma_dict["average_gamma_10000_epochs"],epsilon_dict["average_epsilon_10000_epochs"]),
        "ratio_epsilon_to_alpha_10k": get_ratios(epsilon_dict["average_epsilon_10000_epochs"],alpha_dict["average_alpha_10000_epochs"]),
        "ratio_alpha_to_gamma_30k": get_ratios(alpha_dict["average_alpha_30000_epochs"],gamma_dict["average_gamma_30000_epochs"]),
        "ratio_gamma_to_epsilon_30k": get_ratios(gamma_dict["average_gamma_30000_epochs"],epsilon_dict["average_epsilon_30000_epochs"]),
        "ratio_epsilon_to_alpha_30k": get_ratios(epsilon_dict["average_epsilon_30000_epochs"],alpha_dict["average_alpha_30000_epochs"]),
        "ratio_alpha_to_gamma_50k": get_ratios(alpha_dict["average_alpha_50000_epochs"],gamma_dict["average_gamma_50000_epochs"]),
        "ratio_gamma_to_epsilon_50k": get_ratios(gamma_dict["average_gamma_50000_epochs"],epsilon_dict["average_epsilon_50000_epochs"]),
        "ratio_epsilon_to_alpha_50k": get_ratios(epsilon_dict["average_epsilon_50000_epochs"],alpha_dict["average_alpha_50000_epochs"]),
    
        "ratio_alpha_slope_gamma_slope_1k": get_ratios(alpha_dict["average_alpha_slope_1000_epochs"],gamma_dict["average_gamma_slope_1000_epochs"]),
        "ratio_gamma_slope_epsilon_slope_1k": get_ratios(gamma_dict["average_gamma_slope_1000_epochs"],epsilon_dict["average_epsilon_slope_1000_epochs"]),
        "ratio_epsilon_slope_alpha_slope_1k": get_ratios(epsilon_dict["average_epsilon_slope_1000_epochs"],alpha_dict["average_alpha_slope_1000_epochs"]),
        "ratio_alpha_slope_gamma_slope_5k": get_ratios(alpha_dict["average_alpha_slope_5000_epochs"],gamma_dict["average_gamma_slope_5000_epochs"]),
        "ratio_gamma_slope_epsilon_slope_5k": get_ratios(gamma_dict["average_gamma_slope_5000_epochs"],epsilon_dict["average_epsilon_slope_5000_epochs"]),
        "ratio_epsilon_slope_alpha_slope_5k": get_ratios(epsilon_dict["average_epsilon_slope_5000_epochs"],alpha_dict["average_alpha_slope_5000_epochs"]),
        "ratio_alpha_slope_gamma_slope_10k": get_ratios(alpha_dict["average_alpha_slope_10000_epochs"],gamma_dict["average_gamma_slope_10000_epochs"]),
        "ratio_gamma_slope_epsilon_slope_10k": get_ratios(gamma_dict["average_gamma_slope_10000_epochs"],epsilon_dict["average_epsilon_slope_10000_epochs"]),
        "ratio_epsilon_slope_alpha_slope_10k": get_ratios(epsilon_dict["average_epsilon_slope_10000_epochs"],alpha_dict["average_alpha_slope_10000_epochs"]),
        "ratio_alpha_slope_gamma_slope_30k": get_ratios(alpha_dict["average_alpha_slope_30000_epochs"],gamma_dict["average_gamma_slope_30000_epochs"]),
        "ratio_gamma_slope_epsilon_slope_30k": get_ratios(gamma_dict["average_gamma_slope_30000_epochs"],epsilon_dict["average_epsilon_slope_30000_epochs"]),
        "ratio_epsilon_slope_alpha_slope_30k": get_ratios(epsilon_dict["average_epsilon_slope_30000_epochs"],alpha_dict["average_alpha_slope_30000_epochs"]),
        "ratio_alpha_slope_gamma_slope_50k": get_ratios(alpha_dict["average_alpha_slope_50000_epochs"],gamma_dict["average_gamma_slope_50000_epochs"]),
        "ratio_gamma_slope_epsilon_slope_50k": get_ratios(gamma_dict["average_gamma_slope_50000_epochs"],epsilon_dict["average_epsilon_slope_50000_epochs"]),
        "ratio_epsilon_slope_alpha_slope_50k": get_ratios(epsilon_dict["average_epsilon_slope_50000_epochs"],alpha_dict["average_alpha_slope_50000_epochs"]),
    }
    
    return alpha_dict, gamma_dict, epsilon_dict, ratio_dict




def etl_linear_hparam_features(model):
    
    #epochs_to_base_reward_convergence = rewards_convergence_occurs(model["base_reward_totals"])
    #epochs_to_adjusted_reward_convergence = rewards_convergence_occurs(model["custom_reward_totals"])
    #pdb.set_trace()
    #epochs_to_penalty_convergence = penalty_convergence_occurs(model["penalty_counts"])
    #epochs_to_steps_convergence = steps_convergence_occurs(model["epoch_step_counts"])
    
    
    # some cheese to avoid NoneTypeErrors when we do multiplication, below
    #for epoch_mark in [epochs_to_base_reward_convergence, epochs_to_adjusted_reward_convergence,
    #                   epochs_to_penalty_convergence, epochs_to_steps_convergence]:
    #    if not epoch_mark:
    #        epoch_mark=0.
            

    #pdb.set_trace()
        
    # alpha features and alpha diffs
    alpha_dict = {
        "initial_alpha": model["initial_alpha"],
        "average_alpha_1000_epochs": model["initial_alpha"]+(model["alpha_slope"]*1000),
        "average_alpha_5000_epochs": model["initial_alpha"]+(model["alpha_slope"]*5000),
        "average_alpha_10000_epochs": model["initial_alpha"]+(model["alpha_slope"]*10000),
        "average_alpha_30000_epochs": model["initial_alpha"]+(model["alpha_slope"]*30000),
        "average_alpha_50000_epochs": model["initial_alpha"]+(model["alpha_slope"]*50000),
        #"average_alpha_at_base_rewards_convergence": model["initial_alpha"]+(model["alpha_slope"]*epochs_to_base_reward_convergence),
        #"average_alpha_at_adjusted_rewards_convergence": model["initial_alpha"]+(model["alpha_slope"]*epochs_to_adjusted_reward_convergence),
        #"average_alpha_at_penalty_convergence": model["initial_alpha"]+(model["alpha_slope"]*epochs_to_penalty_convergence),
        #"average_alpha_at_steps_convergence": model["initial_alpha"]+(model["alpha_slope"]*epochs_to_steps_convergence),
        
        "average_alpha_slope_1000_epochs": model["alpha_slope"],
        "average_alpha_slope_5000_epochs": model["alpha_slope"],
        "average_alpha_slope_10000_epochs": model["alpha_slope"],
        "average_alpha_slope_30000_epochs": model["alpha_slope"],
        "average_alpha_slope_50000_epochs": model["alpha_slope"],
        #"average_alpha_slope_at_base_rewards_convergence": model["alpha_slope"],
        #"average_alpha_slope_at_adjusted_rewards_convergence": model["alpha_slope"],
        #"average_alpha_slope_at_penalty_convergence": model["alpha_slope"], 
        #"average_alpha_slope_at_steps_convergence": model["alpha_slope"],
        }


    # gamma features and gamma diffs
    gamma_dict = {
        "initial_gamma": model["initial_gamma"],
        "average_gamma_1000_epochs": model["initial_gamma"]+(model["gamma_slope"]*1000),
        "average_gamma_5000_epochs": model["initial_gamma"]+(model["gamma_slope"]*5000),
        "average_gamma_10000_epochs": model["initial_gamma"]+(model["gamma_slope"]*10000),
        "average_gamma_30000_epochs": model["initial_gamma"]+(model["gamma_slope"]*30000),
        "average_gamma_50000_epochs": model["initial_gamma"]+(model["gamma_slope"]*50000),
        #"average_gamma_at_base_rewards_convergence": model["initial_gamma"]+(model["gamma_slope"]*epochs_to_base_reward_convergence),
        #"average_gamma_at_adjusted_rewards_convergence": model["initial_gamma"]+(model["gamma_slope"]*epochs_to_adjusted_reward_convergence),
        #"average_gamma_at_penalty_convergence": model["initial_gamma"]+(model["gamma_slope"]*epochs_to_penalty_convergence),
        #"average_gamma_at_steps_convergence": model["initial_gamma"]+(model["gamma_slope"]*epochs_to_steps_convergence),
        
        "average_gamma_slope_1000_epochs": model["gamma_slope"],
        "average_gamma_slope_5000_epochs": model["gamma_slope"],
        "average_gamma_slope_10000_epochs": model["gamma_slope"],
        "average_gamma_slope_30000_epochs": model["gamma_slope"],
        "average_gamma_slope_50000_epochs": model["gamma_slope"],
        #"average_gamma_slope_at_base_rewards_convergence": model["gamma_slope"],
        #"average_gamma_slope_at_adjusted_rewards_convergence": model["gamma_slope"],
        #"average_gamma_slope_at_penalty_convergence": model["gamma_slope"], 
        #"average_gamma_slope_at_steps_convergence": model["gamma_slope"],
        }


    # epsilon features and epsilon diffs
    epsilon_dict = {
    "initial_epsilon": model["initial_epsilon"],
    "average_epsilon_1000_epochs": model["initial_epsilon"],
    "average_epsilon_5000_epochs": model["initial_epsilon"],
    "average_epsilon_10000_epochs": model["initial_epsilon"],
    "average_epsilon_30000_epochs": model["initial_epsilon"],
    "average_epsilon_50000_epochs": model["initial_epsilon"],
    #"average_epsilon_at_base_rewards_convergence": model["initial_epsilon"]+(model["epsilon_slope"]*epochs_to_base_reward_convergence),
    #"average_epsilon_at_adjusted_rewards_convergence": model["initial_epsilon"]+(model["epsilon_slope"]*epochs_to_adjusted_reward_convergence),
    #"average_epsilon_at_penalty_convergence": model["initial_epsilon"]+(model["epsilon_slope"]*epochs_to_penalty_convergence),
    #"average_epsilon_at_steps_convergence": model["initial_epsilon"]+(model["epsilon_slope"]*epochs_to_steps_convergence),
        
    "average_epsilon_slope_1000_epochs": model["epsilon_slope"],
    "average_epsilon_slope_5000_epochs": model["epsilon_slope"],
    "average_epsilon_slope_10000_epochs": model["epsilon_slope"],
    "average_epsilon_slope_30000_epochs": model["epsilon_slope"],
    "average_epsilon_slope_50000_epochs": model["epsilon_slope"],
    #"average_epsilon_slope_at_base_rewards_convergence": model["epsilon_slope"],
    #"average_epsilon_slope_at_adjusted_rewards_convergence": model["epsilon_slope"],
    #"average_epsilon_slope_at_penalty_convergence": model["epsilon_slope"], 
    #"average_epsilon_slope_at_steps_convergence": model["epsilon_slope"],
    }
    
    



    
    ratio_dict = {
        "ratio_alpha_to_gamma_initial": get_ratios(alpha_dict["initial_alpha"],gamma_dict["initial_gamma"]),
        "ratio_gamma_to_epsilon_initial": get_ratios(gamma_dict["initial_gamma"],epsilon_dict["initial_epsilon"]),
        "ratio_epsilon_to_alpha_initial": get_ratios(epsilon_dict["initial_epsilon"],alpha_dict["initial_alpha"]),
        "ratio_alpha_to_gamma_5k": get_ratios(alpha_dict["average_alpha_5000_epochs"],gamma_dict["average_gamma_5000_epochs"]),
        "ratio_gamma_to_epsilon_5k": get_ratios(gamma_dict["average_gamma_5000_epochs"],epsilon_dict["average_epsilon_5000_epochs"]),
        "ratio_epsilon_to_alpha_5k": get_ratios(epsilon_dict["average_epsilon_5000_epochs"],alpha_dict["average_alpha_5000_epochs"]),
        "ratio_alpha_to_gamma_10k": get_ratios(alpha_dict["average_alpha_10000_epochs"],gamma_dict["average_gamma_10000_epochs"]),
        "ratio_gamma_to_epsilon_10k": get_ratios(gamma_dict["average_gamma_10000_epochs"],epsilon_dict["average_epsilon_10000_epochs"]),
        "ratio_epsilon_to_alpha_10k": get_ratios(epsilon_dict["average_epsilon_10000_epochs"],alpha_dict["average_alpha_10000_epochs"]),
        "ratio_alpha_to_gamma_30k": get_ratios(alpha_dict["average_alpha_30000_epochs"],gamma_dict["average_gamma_30000_epochs"]),
        "ratio_gamma_to_epsilon_30k": get_ratios(gamma_dict["average_gamma_30000_epochs"],epsilon_dict["average_epsilon_30000_epochs"]),
        "ratio_epsilon_to_alpha_30k": get_ratios(epsilon_dict["average_epsilon_30000_epochs"],alpha_dict["average_alpha_30000_epochs"]),
        "ratio_alpha_to_gamma_50k": get_ratios(alpha_dict["average_alpha_50000_epochs"],gamma_dict["average_gamma_50000_epochs"]),
        "ratio_gamma_to_epsilon_50k": get_ratios(gamma_dict["average_gamma_50000_epochs"],epsilon_dict["average_epsilon_50000_epochs"]),
        "ratio_epsilon_to_alpha_50k": get_ratios(epsilon_dict["average_epsilon_50000_epochs"],alpha_dict["average_alpha_50000_epochs"]),
    
        "ratio_alpha_slope_gamma_slope_1k": get_ratios(alpha_dict["average_alpha_slope_1000_epochs"],gamma_dict["average_gamma_slope_1000_epochs"]),
        "ratio_gamma_slope_epsilon_slope_1k": get_ratios(gamma_dict["average_gamma_slope_1000_epochs"],epsilon_dict["average_epsilon_slope_1000_epochs"]),
        "ratio_epsilon_slope_alpha_slope_1k": get_ratios(epsilon_dict["average_epsilon_slope_1000_epochs"],alpha_dict["average_alpha_slope_1000_epochs"]),
        "ratio_alpha_slope_gamma_slope_5k": get_ratios(alpha_dict["average_alpha_slope_5000_epochs"],gamma_dict["average_gamma_slope_5000_epochs"]),
        "ratio_gamma_slope_epsilon_slope_5k": get_ratios(gamma_dict["average_gamma_slope_5000_epochs"],epsilon_dict["average_epsilon_slope_5000_epochs"]),
        "ratio_epsilon_slope_alpha_slope_5k": get_ratios(epsilon_dict["average_epsilon_slope_5000_epochs"],alpha_dict["average_alpha_slope_5000_epochs"]),
        "ratio_alpha_slope_gamma_slope_10k": get_ratios(alpha_dict["average_alpha_slope_10000_epochs"],gamma_dict["average_gamma_slope_10000_epochs"]),
        "ratio_gamma_slope_epsilon_slope_10k": get_ratios(gamma_dict["average_gamma_slope_10000_epochs"],epsilon_dict["average_epsilon_slope_10000_epochs"]),
        "ratio_epsilon_slope_alpha_slope_10k": get_ratios(epsilon_dict["average_epsilon_slope_10000_epochs"],alpha_dict["average_alpha_slope_10000_epochs"]),
        "ratio_alpha_slope_gamma_slope_30k": get_ratios(alpha_dict["average_alpha_slope_30000_epochs"],gamma_dict["average_gamma_slope_30000_epochs"]),
        "ratio_gamma_slope_epsilon_slope_30k": get_ratios(gamma_dict["average_gamma_slope_30000_epochs"],epsilon_dict["average_epsilon_slope_30000_epochs"]),
        "ratio_epsilon_slope_alpha_slope_30k": get_ratios(epsilon_dict["average_epsilon_slope_30000_epochs"],alpha_dict["average_alpha_slope_30000_epochs"]),
        "ratio_alpha_slope_gamma_slope_50k": get_ratios(alpha_dict["average_alpha_slope_50000_epochs"],gamma_dict["average_gamma_slope_50000_epochs"]),
        "ratio_gamma_slope_epsilon_slope_50k": get_ratios(gamma_dict["average_gamma_slope_50000_epochs"],epsilon_dict["average_epsilon_slope_50000_epochs"]),
        "ratio_epsilon_slope_alpha_slope_50k": get_ratios(epsilon_dict["average_epsilon_slope_50000_epochs"],alpha_dict["average_alpha_slope_50000_epochs"]),
    }
    
    return alpha_dict, gamma_dict, epsilon_dict, ratio_dict



def etl_dynamic_hparam_features(model):
    
    #epochs_to_base_reward_convergence = rewards_convergence_occurs(model["base_reward_totals"])
    #epochs_to_adjusted_reward_convergence = rewards_convergence_occurs(model["custom_reward_totals"])
    #pdb.set_trace()
    #epochs_to_penalty_convergence = penalty_convergence_occurs(model["penalty_counts"])
    #epochs_to_steps_convergence = steps_convergence_occurs(model["epoch_step_counts"])
    
    
    # some cheese to avoid NoneTypeErrors when we do multiplication, below
    #for epoch_mark in [epochs_to_base_reward_convergence, epochs_to_adjusted_reward_convergence,
    #                   epochs_to_penalty_convergence, epochs_to_steps_convergence]:
    #    if not epoch_mark:
    #        epoch_mark=0.

        
    # alpha features and alpha diffs
    alpha_dict = {
        "initial_alpha": model["alpha_by_epoch"][0],
        "average_alpha_1000_epochs": metric_avg_at_mark(model["alpha_by_epoch"],1000),
        "average_alpha_5000_epochs": metric_avg_at_mark(model["alpha_by_epoch"],5000),
        "average_alpha_10000_epochs": metric_avg_at_mark(model["alpha_by_epoch"],10000),
        "average_alpha_30000_epochs": metric_avg_at_mark(model["alpha_by_epoch"],30000),
        "average_alpha_50000_epochs": metric_avg_at_mark(model["alpha_by_epoch"],50000),
        #"average_alpha_at_base_rewards_convergence": metric_avg_at_mark(model["alpha_by_epoch"],
        #                                                               epochs_to_base_reward_convergence),
        #"average_alpha_at_adjusted_rewards_convergence": metric_avg_at_mark(model["alpha_by_epoch"],
        #                                                                   epochs_to_adjusted_reward_convergence),
        #"average_alpha_at_penalty_convergence": metric_avg_at_mark(model["alpha_by_epoch"],
        #                                                          epochs_to_penalty_convergence),
        #"average_alpha_at_steps_convergence": metric_avg_at_mark(model["alpha_by_epoch"],
        #                                                        epochs_to_steps_convergence),
        "average_alpha_slope_1000_epochs": metric_slope_at_mark(model["alpha_by_epoch"],1000),
        "average_alpha_slope_5000_epochs": metric_slope_at_mark(model["alpha_by_epoch"],5000),
        "average_alpha_slope_10000_epochs": metric_slope_at_mark(model["alpha_by_epoch"],10000),
        "average_alpha_slope_30000_epochs": metric_slope_at_mark(model["alpha_by_epoch"],30000),
        "average_alpha_slope_50000_epochs": metric_slope_at_mark(model["alpha_by_epoch"],50000),
        #"average_alpha_slope_at_base_rewards_convergence": metric_slope_at_mark(model["alpha_by_epoch"],
        #                                                                       epochs_to_base_reward_convergence),
        #"average_alpha_slope_at_adjusted_rewards_convergence": metric_slope_at_mark(model["alpha_by_epoch"],
        #                                                                           epochs_to_adjusted_reward_convergence),
        #"average_alpha_slope_at_penalty_convergence": metric_slope_at_mark(model["alpha_by_epoch"], 
        #                                                                  epochs_to_penalty_convergence),
        #"average_alpha_slope_at_steps_convergence": metric_slope_at_mark(model["alpha_by_epoch"],
        #                                                                epochs_to_steps_convergence)
        }


    # gamma features and gamma diffs
    gamma_dict = {
        "initial_gamma": model["gamma_by_epoch"][0],
        "average_gamma_1000_epochs": metric_avg_at_mark(model["gamma_by_epoch"],1000),
        "average_gamma_5000_epochs": metric_avg_at_mark(model["gamma_by_epoch"],5000),
        "average_gamma_10000_epochs": metric_avg_at_mark(model["gamma_by_epoch"],10000),
        "average_gamma_30000_epochs": metric_avg_at_mark(model["gamma_by_epoch"],30000),
        "average_gamma_50000_epochs": metric_avg_at_mark(model["gamma_by_epoch"],50000),
        #"average_gamma_at_base_rewards_convergence": metric_avg_at_mark(model["gamma_by_epoch"],
        #                                                               epochs_to_base_reward_convergence),
        #"average_gamma_at_adjusted_rewards_convergence": metric_avg_at_mark(model["gamma_by_epoch"],
        #                                                                   epochs_to_adjusted_reward_convergence),
        #"average_gamma_at_penalty_convergence": metric_avg_at_mark(model["gamma_by_epoch"],
        #                                                          epochs_to_penalty_convergence),
        #"average_gamma_at_steps_convergence": metric_avg_at_mark(model["gamma_by_epoch"],
        #                                                        epochs_to_steps_convergence),
        "average_gamma_slope_1000_epochs": metric_slope_at_mark(model["gamma_by_epoch"], 1000),
        "average_gamma_slope_5000_epochs": metric_slope_at_mark(model["gamma_by_epoch"], 5000),
        "average_gamma_slope_10000_epochs": metric_slope_at_mark(model["gamma_by_epoch"], 10000),
        "average_gamma_slope_30000_epochs": metric_slope_at_mark(model["gamma_by_epoch"], 30000),
        "average_gamma_slope_50000_epochs": metric_slope_at_mark(model["gamma_by_epoch"], 50000),
        #"average_gamma_slope_at_base_rewards_convergence": metric_slope_at_mark(model["gamma_by_epoch"],
        #                                                                       epochs_to_base_reward_convergence),
        #"average_gamma_slope_at_adjusted_rewards_convergence": metric_slope_at_mark(model["gamma_by_epoch"],
        #                                                                           epochs_to_adjusted_reward_convergence),
        #"average_gamma_slope_at_penalty_convergence": metric_slope_at_mark(model["gamma_by_epoch"], 
        #                                                                  epochs_to_penalty_convergence),
        #"average_gamma_slope_at_steps_convergence": metric_slope_at_mark(model["alpha_by_epoch"],
        #                                                                epochs_to_steps_convergence)
        }


    # epsilon features and epsilon diffs
    epsilon_dict = {
    "initial_epsilon": model["epsilon_by_epoch"][0],
    "average_epsilon_1000_epochs": metric_avg_at_mark(model["epsilon_by_epoch"],1000),
    "average_epsilon_5000_epochs": metric_avg_at_mark(model["epsilon_by_epoch"],5000),
    "average_epsilon_10000_epochs": metric_avg_at_mark(model["epsilon_by_epoch"],10000),
    "average_epsilon_30000_epochs": metric_avg_at_mark(model["epsilon_by_epoch"],30000),
    "average_epsilon_50000_epochs": metric_avg_at_mark(model["epsilon_by_epoch"],50000),
    #"average_epsilon_at_base_rewards_convergence": metric_avg_at_mark(model["epsilon_by_epoch"],
    #                                                                 epochs_to_base_reward_convergence),
    #"average_epsilon_at_adjusted_rewards_convergence": metric_avg_at_mark(model["epsilon_by_epoch"],
    #                                                                     epochs_to_adjusted_reward_convergence),
    #"average_epsilon_at_penalty_convergence": metric_avg_at_mark(model["epsilon_by_epoch"],
    #                                                            epochs_to_penalty_convergence),
    #"average_epsilon_at_steps_convergence": metric_avg_at_mark(model["epsilon_by_epoch"],
    #                                                          epochs_to_steps_convergence),
    "average_epsilon_slope_1000_epochs": metric_slope_at_mark(model["epsilon_by_epoch"], 1000),
    "average_epsilon_slope_5000_epochs": metric_slope_at_mark(model["epsilon_by_epoch"], 5000),
    "average_epsilon_slope_10000_epochs": metric_slope_at_mark(model["epsilon_by_epoch"], 10000),
    "average_epsilon_slope_30000_epochs": metric_slope_at_mark(model["epsilon_by_epoch"], 30000),
    "average_epsilon_slope_50000_epochs": metric_slope_at_mark(model["epsilon_by_epoch"], 50000),
    #"average_epsilon_slope_at_base_rewards_convergence": metric_slope_at_mark(model["epsilon_by_epoch"],
    #                                                                         epochs_to_base_reward_convergence),
    #"average_epsilon_slope_at_adjusted_rewards_convergence": metric_slope_at_mark(model["epsilon_by_epoch"],
    #                                                                             epochs_to_adjusted_reward_convergence),
    #"average_epsilon_slope_at_penalty_convergence": metric_slope_at_mark(model["epsilon_by_epoch"], 
    #                                                                    epochs_to_penalty_convergence),
    #"average_epsilon_slope_at_steps_convergence": metric_slope_at_mark(model["epsilon_by_epoch"],
    #                                                                  epochs_to_steps_convergence)
    }
    
    ratio_dict = {
        "ratio_alpha_to_gamma_initial": get_ratios(alpha_dict["initial_alpha"],gamma_dict["initial_gamma"]),
        "ratio_gamma_to_epsilon_initial": get_ratios(gamma_dict["initial_gamma"],epsilon_dict["initial_epsilon"]),
        "ratio_epsilon_to_alpha_initial": get_ratios(epsilon_dict["initial_epsilon"],alpha_dict["initial_alpha"]),
        "ratio_alpha_to_gamma_5k": get_ratios(alpha_dict["average_alpha_5000_epochs"],gamma_dict["average_gamma_5000_epochs"]),
        "ratio_gamma_to_epsilon_5k": get_ratios(gamma_dict["average_gamma_5000_epochs"],epsilon_dict["average_epsilon_5000_epochs"]),
        "ratio_epsilon_to_alpha_5k": get_ratios(epsilon_dict["average_epsilon_5000_epochs"],alpha_dict["average_alpha_5000_epochs"]),
        "ratio_alpha_to_gamma_10k": get_ratios(alpha_dict["average_alpha_10000_epochs"],gamma_dict["average_gamma_10000_epochs"]),
        "ratio_gamma_to_epsilon_10k": get_ratios(gamma_dict["average_gamma_10000_epochs"],epsilon_dict["average_epsilon_10000_epochs"]),
        "ratio_epsilon_to_alpha_10k": get_ratios(epsilon_dict["average_epsilon_10000_epochs"],alpha_dict["average_alpha_10000_epochs"]),
        "ratio_alpha_to_gamma_30k": get_ratios(alpha_dict["average_alpha_30000_epochs"],gamma_dict["average_gamma_30000_epochs"]),
        "ratio_gamma_to_epsilon_30k": get_ratios(gamma_dict["average_gamma_30000_epochs"],epsilon_dict["average_epsilon_30000_epochs"]),
        "ratio_epsilon_to_alpha_30k": get_ratios(epsilon_dict["average_epsilon_30000_epochs"],alpha_dict["average_alpha_30000_epochs"]),
        "ratio_alpha_to_gamma_50k": get_ratios(alpha_dict["average_alpha_50000_epochs"],gamma_dict["average_gamma_50000_epochs"]),
        "ratio_gamma_to_epsilon_50k": get_ratios(gamma_dict["average_gamma_50000_epochs"],epsilon_dict["average_epsilon_50000_epochs"]),
        "ratio_epsilon_to_alpha_50k": get_ratios(epsilon_dict["average_epsilon_50000_epochs"],alpha_dict["average_alpha_50000_epochs"]),
    
        "ratio_alpha_slope_gamma_slope_1k": get_ratios(alpha_dict["average_alpha_slope_1000_epochs"],gamma_dict["average_gamma_slope_1000_epochs"]),
        "ratio_gamma_slope_epsilon_slope_1k": get_ratios(gamma_dict["average_gamma_slope_1000_epochs"],epsilon_dict["average_epsilon_slope_1000_epochs"]),
        "ratio_epsilon_slope_alpha_slope_1k": get_ratios(epsilon_dict["average_epsilon_slope_1000_epochs"],alpha_dict["average_alpha_slope_1000_epochs"]),
        "ratio_alpha_slope_gamma_slope_5k": get_ratios(alpha_dict["average_alpha_slope_5000_epochs"],gamma_dict["average_gamma_slope_5000_epochs"]),
        "ratio_gamma_slope_epsilon_slope_5k": get_ratios(gamma_dict["average_gamma_slope_5000_epochs"],epsilon_dict["average_epsilon_slope_5000_epochs"]),
        "ratio_epsilon_slope_alpha_slope_5k": get_ratios(epsilon_dict["average_epsilon_slope_5000_epochs"],alpha_dict["average_alpha_slope_5000_epochs"]),
        "ratio_alpha_slope_gamma_slope_10k": get_ratios(alpha_dict["average_alpha_slope_10000_epochs"],gamma_dict["average_gamma_slope_10000_epochs"]),
        "ratio_gamma_slope_epsilon_slope_10k": get_ratios(gamma_dict["average_gamma_slope_10000_epochs"],epsilon_dict["average_epsilon_slope_10000_epochs"]),
        "ratio_epsilon_slope_alpha_slope_10k": get_ratios(epsilon_dict["average_epsilon_slope_10000_epochs"],alpha_dict["average_alpha_slope_10000_epochs"]),
        "ratio_alpha_slope_gamma_slope_30k": get_ratios(alpha_dict["average_alpha_slope_30000_epochs"],gamma_dict["average_gamma_slope_30000_epochs"]),
        "ratio_gamma_slope_epsilon_slope_30k": get_ratios(gamma_dict["average_gamma_slope_30000_epochs"],epsilon_dict["average_epsilon_slope_30000_epochs"]),
        "ratio_epsilon_slope_alpha_slope_30k": get_ratios(epsilon_dict["average_epsilon_slope_30000_epochs"],alpha_dict["average_alpha_slope_30000_epochs"]),
        "ratio_alpha_slope_gamma_slope_50k": get_ratios(alpha_dict["average_alpha_slope_50000_epochs"],gamma_dict["average_gamma_slope_50000_epochs"]),
        "ratio_gamma_slope_epsilon_slope_50k": get_ratios(gamma_dict["average_gamma_slope_50000_epochs"],epsilon_dict["average_epsilon_slope_50000_epochs"]),
        "ratio_epsilon_slope_alpha_slope_50k": get_ratios(epsilon_dict["average_epsilon_slope_50000_epochs"],alpha_dict["average_alpha_slope_50000_epochs"]),
    }


    return alpha_dict, gamma_dict, epsilon_dict, ratio_dict


#### LOAD DIFFERENTIAL FEATURES ####

def static_differential_features(static_model):
    
    
    alpha_series = [static_model["initial_alpha"] for i in range(60000)]
    alpha_deriv = [0. for i in range(60000)]
    gamma_series = [static_model["initial_gamma"] for i in range(60000)]
    gamma_deriv = [0. for i in range(60000)]
    epsilon_series = [static_model["initial_epsilon"] for i in range(60000)]
    epsilon_deriv = [0. for i in range(60000)]
    
    penalty_series = static_model["penalty_counts"]
    penalty_deriv = 0.
    base_reward_series = static_model["base_reward_totals"]
    base_reward_deriv = 0.
    adjusted_reward_series = static_model["custom_reward_totals"]
    adjusted_reward_deriv = 0.
    steps_series = static_model["epoch_step_counts"]
    steps_deriv = 0.
    
    
    differential_dict = {
        #"d_alpha_d_t": 0.,
        #"d_gamma_d_t": 0.,
        #"d_epsilon_d_t": 0.,
        
        "d_epsilon_d_reward": 0.,
        "d_epsilon_d_penalty": 0.,
        "d_epsilon_d_steps": 0.,

        "d_alpha_d_reward": 0.,
        "d_alpha_d_penalty": 0.,
        "d_alpha_d_steps": 0.,

        "d_gamma_d_reward": 0.,
        "d_gamma_d_penalty": 0.,
        "d_gamma_d_steps": 0.,

        "sign_d_epsilon_d_reward": 0.,
        "sign_d_epsilon_d_penalty": 0.,
        "sign_d_epsilon_d_steps": 0.,

        "sign_d_alpha_d_reward": 0.,
        "sign_d_alpha_d_penalty": 0.,
        "sign_d_alpha_d_steps": 0.,

        "sign_d_gamma_d_reward": 0.,
        "sign_d_gamma_d_penalty": 0.,
        "sign_d_gamma_d_steps": 0.,
    }
    
    return differential_dict


# differential relationships



def linear_differential_features(linear_model):
    
    alpha_slope = linear_model["alpha_slope"]
    gamma_slope = linear_model["gamma_slope"]
    epsilon_slope = linear_model["epsilon_slope"]
    
    alpha_series = [linear_model["initial_alpha"]+(i*alpha_slope) for i in range(60000)]
    alpha_deriv = [alpha_slope for i in range(60000)]
    gamma_series = [linear_model["initial_gamma"]+(i*gamma_slope) for i in range(60000)]
    gamma_deriv = [gamma_slope for i in range(60000)]
    epsilon_series = [linear_model["initial_epsilon"]+(i*epsilon_slope) for i in range(60000)]
    epsilon_deriv = [epsilon_slope for i in range(60000)]
    
    penalty_series = linear_model["penalty_counts"]
    penalty_deriv = 0.
    base_reward_series = linear_model["base_reward_totals"]
    base_reward_deriv = 0.
    adjusted_reward_series = linear_model["custom_reward_totals"]
    adjusted_reward_deriv = 0.
    steps_series = linear_model["epoch_step_counts"]
    steps_deriv = 0.
    
    differential_dict = {
        #"d_alpha_d_t": linear_model["alpha_slope"],
        #"d_gamma_d_t": linear_model["gamma_slope"],
        #"d_epsilon_d_t": linear_model["epsilon_slope"],
        
        "d_epsilon_d_reward": 0.,
        "d_epsilon_d_penalty": 0.,
        "d_epsilon_d_steps": 0.,

        "d_alpha_d_reward": 0.,
        "d_alpha_d_penalty": 0.,
        "d_alpha_d_steps": 0.,

        "d_gamma_d_reward": 0.,
        "d_gamma_d_penalty": 0.,
        "d_gamma_d_steps": 0.,

        "sign_d_epsilon_d_reward": 0.,
        "sign_d_epsilon_d_penalty": 0.,
        "sign_d_epsilon_d_steps": 0.,

        "sign_d_alpha_d_reward": 0.,
        "sign_d_alpha_d_penalty": 0.,
        "sign_d_alpha_d_steps": 0.,

        "sign_d_gamma_d_reward": 0.,
        "sign_d_gamma_d_penalty": 0.,
        "sign_d_gamma_d_steps": 0.,
    }
    
    return differential_dict


def dynamic_differential_features(dynamic_model):
    
    alpha_series = dynamic_model["alpha_by_epoch"]
    alpha_deriv = [0. for i in range(60000)]
    gamma_series = dynamic_model["gamma_by_epoch"]
    gamma_deriv = [0. for i in range(60000)]
    epsilon_series = dynamic_model["epsilon_by_epoch"]
    epsilon_deriv = [0. for i in range(60000)]
    
    penalty_series = dynamic_model["penalty_counts"]
    penalty_deriv = 0.
    base_reward_series = dynamic_model["base_reward_totals"]
    base_reward_deriv = 0.
    adjusted_reward_series = dynamic_model["custom_reward_totals"]
    adjusted_reward_deriv = 0.
    steps_series = dynamic_model["epoch_step_counts"]
    steps_deriv = 0.
    
    
    differential_dict = {
        #"d_alpha_d_t": linear_model["alpha_slope"],
        #"d_gamma_d_t": linear_model["gamma_slope"],
        #"d_epsilon_d_t": linear_model["epsilon_slope"],
        
        "d_epsilon_d_reward": (dynamic_model["epsilon_metaparams"]["reward_differential"][0]*
                               dynamic_model["epsilon_metaparams"]["reward_differential"][1]),
        "d_epsilon_d_penalty": (dynamic_model["epsilon_metaparams"]["penalties_differential"][0]*
                                dynamic_model["epsilon_metaparams"]["penalties_differential"][1]),
        "d_epsilon_d_steps": (dynamic_model["epsilon_metaparams"]["steps_differential"][0]*
                              dynamic_model["epsilon_metaparams"]["steps_differential"][1]),
        
        "d_alpha_d_reward": (dynamic_model["alpha_metaparams"]["reward_differential"][0]*
                             dynamic_model["alpha_metaparams"]["reward_differential"][1]),
        "d_alpha_d_penalty": (dynamic_model["alpha_metaparams"]["penalties_differential"][0]*
                              dynamic_model["alpha_metaparams"]["penalties_differential"][1]),
        "d_alpha_d_steps": (dynamic_model["alpha_metaparams"]["steps_differential"][0]*
                            dynamic_model["alpha_metaparams"]["steps_differential"][1]),

        "d_gamma_d_reward": (dynamic_model["gamma_metaparams"]["reward_differential"][0]*
                             dynamic_model["gamma_metaparams"]["reward_differential"][1]),
        "d_gamma_d_penalty": (dynamic_model["gamma_metaparams"]["penalties_differential"][0]*
                              dynamic_model["gamma_metaparams"]["penalties_differential"][1]),
        "d_gamma_d_steps": (dynamic_model["gamma_metaparams"]["steps_differential"][0]*
                            dynamic_model["gamma_metaparams"]["steps_differential"][1]),

        "sign_d_epsilon_d_reward": dynamic_model["epsilon_metaparams"]["reward_differential"][1],
        "sign_d_epsilon_d_penalty": dynamic_model["epsilon_metaparams"]["penalties_differential"][1],
        "sign_d_epsilon_d_steps": dynamic_model["epsilon_metaparams"]["steps_differential"][1],

        "sign_d_alpha_d_reward": dynamic_model["alpha_metaparams"]["reward_differential"][1],
        "sign_d_alpha_d_penalty": dynamic_model["alpha_metaparams"]["penalties_differential"][1],
        "sign_d_alpha_d_steps": dynamic_model["alpha_metaparams"]["steps_differential"][1],

        "sign_d_gamma_d_reward": dynamic_model["gamma_metaparams"]["reward_differential"][1],
        "sign_d_gamma_d_penalty": dynamic_model["gamma_metaparams"]["penalties_differential"][1],
        "sign_d_gamma_d_steps": dynamic_model["gamma_metaparams"]["steps_differential"][1],
    }
    
    return differential_dict




####  ETL PER MODEL TYPE  ####
#### THIS IS THE BIZNAZZ  ####

def etl_static(static_model):
    hparam_features = etl_static_hparam_features(static_model)
    environment_features = etl_static_environment_features(static_model)
    metric_features = etl_metric_features(static_model)
    differential_features = static_differential_features(static_model)
    
    model_dict = {}
    model_dict.update(environment_features)
    for hf in hparam_features:
        model_dict.update(hf)
    for mf in metric_features:
        model_dict.update(mf) 
    model_dict.update(differential_features)

    return model_dict
    
    
def etl_linear(linear_model):
    hparam_features = etl_linear_hparam_features(linear_model)
    environment_features = etl_static_environment_features(linear_model)
    metric_features = etl_metric_features(linear_model)
    differential_features = linear_differential_features(linear_model)
    
    model_dict = {}
    model_dict.update(environment_features)
    for hf in hparam_features:
        model_dict.update(hf)
    for mf in metric_features:
        model_dict.update(mf) 
    model_dict.update(differential_features)
    return model_dict

def etl_dynamic(dynamic_model):
    hparam_features = etl_dynamic_hparam_features(dynamic_model)
    environment_features = etl_dynamic_environment_features(dynamic_model)
    metric_features = etl_metric_features(dynamic_model)
    differential_features = dynamic_differential_features(dynamic_model)
    model_dict = {}
    model_dict.update(environment_features)
    for hf in hparam_features:
        model_dict.update(hf)
    for mf in metric_features:
        model_dict.update(mf) 
    model_dict.update(differential_features)

    return model_dict


def etl_dynamic_diff(dynamic_model_diff_env):
    hparam_features = etl_dynamic_hparam_features(dynamic_model_diff_env)
    environment_features = etl_dynamic_diff_environment_features(dynamic_model_diff_env)
    metric_features = etl_metric_features(dynamic_model_diff_env)
    differential_features = dynamic_differential_features(dynamic_model_diff_env)
    model_dict = {}
    model_dict.update(environment_features)
    for hf in hparam_features:
        model_dict.update(hf)
    for mf in metric_features:
        model_dict.update(mf) 
    model_dict.update(differential_features)

    return model_dict